<?php
/* Set sender */
function job_mgt_set_sender($form, &$form_state) {
	$form = array();

	$form['sender'] = array(
		'#type' => 'textfield',
		'#title' => 'From (Sender)',
		'#description' => 'Specify sender\'s email. It varies based on your domain, ex - jobs@yourdomain.com or courses@yourdomain.com .',
		'#default_value' => 'jobs@example.com',
		'#size' => '50',
		'#required' => TRUE,
	);

	$form['admin'] = array(
		'#type' => 'textfield',
		'#title' => 'Admin (Receiver of ALL)',
		'#description' => 'Specify Admin\'s email in order to receive notification when new applications are been submitted. Default is SiteMail.',
		'#default_value' => variable_get('site_mail',''),
		'#size' => '50',
		'#required' => TRUE,
	);
	
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => 'Save Changes',
		);
	
	$form['reset'] = array(
		'#type' => 'markup',
		'#markup' => '<input type=reset  class=form-submit />'
		);

	$sender = variable_get('job_mgt_sender','NULL');
	if($sender != 'NULL') {
		$form['sender']['#default_value'] = $sender;
	}
	
	return $form;
}

/* Set sender - Validator */
function job_mgt_set_sender_validate($form, &$form_state) {
	$sender = $form_state['values']['sender'];
	if (!filter_var($sender, FILTER_VALIDATE_EMAIL)) {
		form_set_error('sender', t('Please enter valid email-address'));
	}
	$admin = $form_state['values']['admin'];
	if (!filter_var($admin, FILTER_VALIDATE_EMAIL)) {
		form_set_error('admin', t('Please enter valid email-address'));
	}
}

/* Set sender - Handler */
function job_mgt_set_sender_submit($form, &$form_state) {
	
	$sender = $form_state['values']['sender'];
	$admin = $form_state['values']['admin'];

	variable_set('job_mgt_mails',$sender . '#' . $admin);

	drupal_flush_all_caches();
	drupal_set_message('Settings Saved');
}

/*
Setting-up Email body for Employer and Job-Seeker
*/

function job_mgt_email_body($form, &$form_state) {
	
	$ctypes = variable_get('job_mgt_ctypes','NULL');
	if($ctypes != 'NULL') {
		$separate = explode("#",$ctypes['0']);
		
		$job_posting_fields = field_info_instances("node",$separate[0]);
		$job_posting_tokens = "";
		foreach ($job_posting_fields as $field => $value) {
			$job_posting_tokens .= "<br/>{" . $field . '}';			
		}

		$job_seeker_fields = field_info_instances("node",$separate[3]);
		$job_seeker_tokens = "";
		foreach ($job_seeker_fields as $field => $value) {
			$job_seeker_tokens .= "<br/>{" . $field . '}';			
		}

	}
	else {
		$msg = '<span style="color:red">No content-type selected</span>';

		$separate[0] = $msg;
		$separate[3] = $msg;
		$msg = '<span style="color:red"><br/>Please configure content-types @ Configure Desired Fields</span>';

		$job_seeker_tokens = $msg;
		$job_posting_tokens = $msg;
	}

	$form = array();

	$form['employer'] = array(
		'#type' => 'fieldset',
		'#title' => 'Employer\'s',
		'#description' => 'Email Format for Employer',
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
		);

	$form['employer']['emp-name'] = array(
		'#type' => 'markup',
		'#markup' =>'<br/><strong>Name : </strong>Employer', 
		);

	$form['employer']['emp-subject'] = array(
		'#type' => 'textfield',
		'#title' => 'Subject',
		'#size' => 64,
		'#required' => TRUE,
		);

	$form['employer']['emp-body'] = array(
		'#type' => 'textarea',
		'#title' => 'Body',	
		'#description' => 'Use Tokens - Available tokens vary based on your selection',
		'#resizable' => TRUE,
		'#rows' => 15,
		'#required' => TRUE,
		);
	
	$form['employer']['emp-tokens'] = array(
		'#type' => 'markup',
		'#title' => 'Tokens for Employer',
		'#markup' => "<strong> Tokens ($separate[0]) </strong> - 1st Preference" . $job_posting_tokens . "<br/><strong> Tokens ($separate[3]) </strong> - 2nd Preference " . $job_seeker_tokens,
		'#prefix' => "<hr/><br/>",
		'#suffix' => "<br/>",
		);

	$form['employer']['emp-example'] = array(
		'#type' => 'markup',
		'#markup' => "Feel this sooner",
		'#prefix' => "<br/><hr/><br/><strong> Usage Example</strong><br/><pre>",
		'#suffix' => "<br/></pre>",
		);

	$form['job-seeker'] = array(
		'#type' => 'fieldset',
		'#title' => 'Job Seeker\'s',
		'#description' => 'Email Format for Job Seeker',
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
		);
	
	$form['job-seeker']['jobsk-name'] = array(
		'#type' => 'markup',
		'#markup' =>'<br/><strong>Name : </strong>Job-Seeker', 
		);

	$form['job-seeker']['jobsk-subject'] = array(
		'#type' => 'textfield',
		'#title' => 'Subject',
		'#size' => 64,
		'#required' => TRUE,
		);
	
	$form['job-seeker']['jobsk-body'] = array(
		'#type' => 'textarea',
		'#title' => 'Body',	
		'#description' => 'Use Tokens - Available tokens vary based on your selection',
		'#resizable' => TRUE,
		'#rows' => 15,
		'#required' => TRUE,
		);

	$form['job-seeker']['jobsk-tokens'] = array(
		'#type' => 'markup',
		'#title' => 'Tokens for Job Seeker',
		// '#markup' => "<strong> Tokens ($separate[1])</strong> " . $job_seeker_tokens,
		'#markup' => "<strong> Tokens ($separate[3]) </strong> - 1st Preference" . $job_seeker_tokens . "<br/><strong> Tokens ($separate[0]) </strong> - 2nd Preference " . $job_posting_tokens,
		'#prefix' => "<hr/><br/>",
		'#suffix' => "<br/>",
		);

	$form['job-seeker']['jobsk-example'] = array(
		'#type' => 'markup',
		'#markup' => "Fill this sooner",
		'#prefix' => "<br/><hr/><br/><strong> Usage Example</strong><br/><pre>",
		'#suffix' => "<br/></pre>",
		);

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => 'Save Changes',
		);

	$form['reset'] = array(
		'#type' => 'markup',
		'#markup' => '<input type=reset  class=form-submit />',
		);

	$single = db_get_template('employer');
	$form['employer']['emp-subject']['#markup'] = $single->name;
	$form['employer']['emp-subject']['#default_value'] = $single->subject;
	$form['employer']['emp-body']['#default_value'] = $single->body;
	$form['employer']['emp-example']['#markup'] = $single->example;

	$single = db_get_template('job-seeker');
	$form['job-seeker']['jobsk-subject']['#default_value'] = $single->subject;
	$form['job-seeker']['jobsk-body']['#default_value'] = $single->body;
	$form['job-seeker']['jobsk-example']['#markup'] = $single->example;
	
	return $form;
}

/*
* Handling the values
*/
function job_mgt_email_body_submit($form, &$form_state) {

	/* Updating data in custom table */
	$emp_query = db_update('job_mgt_templates')
    ->fields(array(
    	'subject' => $form_state['values']['emp-subject'],
    	'body' => $form_state['values']['emp-body'],
    	))
    ->condition('name', 'employer')
    ->execute();

	$seeker_query = db_update('job_mgt_templates')
    ->fields(array(
    	'subject' => $form_state['values']['jobsk-subject'],
    	'body' => $form_state['values']['jobsk-body'],
    	))
    ->condition('name', 'job-seeker')
    ->execute();

    drupal_flush_all_caches();
    drupal_set_message("Settings Saved");
}

/*
* Loading template from custom drupal table
*/

function db_get_template($name) {
	$result = db_select('job_mgt_templates','jmt')
	->fields('jmt')
	->condition('name',$name,'=')
	->execute()
	->fetchObject();

	return $result;
}
/*
*	Will match the regular expression and will return array of matched tokens.
*/
function filter_token_scan($body) {
  // Matches tokens with the following pattern: {$fieldname}
  // $type may not contain : or whitespace characters, but $name may.
  preg_match_all('/
    \{             # { - pattern start
    ([^\s\[\]:]*)  # match $type not containing whitespace : [ or ]
    \}             # } - pattern end
    /x', $body, $matches);

  $fields = $matches[1];

  // Iterate through the matches, building an associative array containing
  $results = array();
  for ($i = 0; $i < count($fields); $i++) {
  	array_push($results, $matches[0][$i]);
  }

  return $results;
}