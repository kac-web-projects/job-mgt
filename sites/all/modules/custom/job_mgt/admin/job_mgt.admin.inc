<?php

/*
* Attach/set email fields
*/

function job_mgt_content_types($form, &$form_state) {
	$types = node_type_get_types();
	
	$c_types = array();
	foreach ($types as $type) {
	$c_types[$type->type] = $type->type;	
	}
	// dpm($c_types);	
	$form = array();

	$form['job-posting'] = array(
		'#type' => 'fieldset',
		'#title' => 'Posting (Parent)', // Job Posting
		'#description' => 'Set Posting(Parent) content type',
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
		);

	$form['job-posting']['posting-type'] = array(
		'#type' => 'select',
		'#description' => 'Select Content type',
		'#options' => $c_types,
		'#required' => TRUE,
		);

	$form['job-posting']['email-enable'] = array(
		'#type' => 'checkbox',
		'#title' => 'Send email to individual employer?',
		'#description' => 'Each job-posting can have different contactable person',
		'#return_value' => 1,
		);

	$form['job-posting']['posting-field'] = array(
		'#type' => 'fieldset',
		'#title' => 'Set Email',
		'#description' => 'Email field where job-seeker will receive confirmation about job-applied',
		'#collapsible' => TRUE,
		'#states' => array(
			'visible' => array(
				':input[name="email-enable"]' => array('checked' => TRUE),
				),
			),
		);

	$form['job-application'] = array(
		'#type' => 'fieldset',
		'#title' => 'Application (Child)', // Job Application 
		'#description' => 'Set Application(Child) content type',
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);

	$form['job-application']['application-type'] = array(
		'#type' => 'select',
		'#description' => 'Select Content type',
		'#options' => $c_types,
		'#required' => TRUE,
		);

	$form['job-application']['application-field'] = array(
		'#type' => 'fieldset',
		'#title' => 'Set Email',
		'#description' => 'Email field where job-seeker will receive confirmation about job-applied;',
		'#collapsible' => TRUE,
		);

	$form['job-application']['relationship'] = array(
		'#type' => 'fieldset',
		'#title' => 'Set Relationship (Common field to Share)',
		'#description' => 'There must be something common between Parent content-type -> Child content-type',
		'#collapsible' => TRUE,
		);

	foreach($c_types as $type) {
	$fields = field_info_instances("node",$type);
	$type_options = array();
	$type_options['title'] = 'Title';
		foreach ($fields as $field => $value) {
			$type_options[$field] = $field;			
		}
	
			$form['job-application']['relationship']['parent-field#' . $type] = array(
				'#type' => 'select',
				'#title' => $type,
				'#description' => 'Parent(Set reference field to job-seeker\'s form)',
				'#options' => $type_options,
				'#weight' => 0,
				'#states' => array(
					'visible' => array(
						':input[name="posting-type"]' => array('value' => $type),
						),
					'required' => array(
						':input[name="posting-type"]' => array('value' => $type),
						),
					),
			);

			$form['job-posting']['posting-field']['email-' . $type] = array(
				'#type' => 'select',
				'#title' => $type,
				'#description' => 'Individual employer or HR will receive email',
				'#options' => $type_options,
				'#states' => array(
					'visible' => array(
						':input[name="posting-type"]' => array('value' => $type),
						':input[name="email-enable"]' => array('checked' => TRUE),
						),
					'required' => array(
						':input[name="posting-type"]' => array('value' => $type),
						),
					),
			);
			
			$form['job-application']['application-field'][$type] = array(
				'#type' => 'select',
				'#title' => $type,
				'#description' => 'Set Email-field, email for Job Seeker',
				'#options' => $type_options,
				'#states' => array(
					'visible' => array(
						':input[name="application-type"]' => array('value' => $type),
						),
					'required' => array(
						':input[name="application-type"]' => array('value' => $type),
						),
					),
			);

			$form['job-application']['relationship']['child-field#' . $type] = array(
				'#type' => 'select',
				'#title' => $type,
				'#description' => 'Child(Preserve reffered field value from Parent)',
				'#options' => $type_options,
				'#states' => array(
					'visible' => array(
						':input[name="application-type"]' => array('value' => $type),
						),
					'required' => array(
						':input[name="application-type"]' => array('value' => $type),
						),
					),
			);

		}	
	
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => 'Save Changes',
		);
	
	$form['reset'] = array(
		'#type' => 'markup',
		'#markup' => '<input type=reset  class=form-submit />'
		);

	$ctypes = variable_get('job_mgt_ctypes','NULL');
	if($ctypes != 'NULL') {
		$separate = explode("#",$ctypes['0']);

		$form['job-posting']['posting-type']['#default_value'] = $separate[0];
		$form['job-application']['application-type']['#default_value'] = $separate[3];

		$form['job-posting']['email-enable']['#default_value'] = $separate[1];
		$form['job-posting']['posting-field']['email-' . $separate[0]]['#default_value'] = $separate[2];

		$form['job-application']['application-field'][$separate[3]]['#default_value'] = $separate[4];
		$form['job-application']['relationship']['parent-field#' . $separate[0]]['#default_value'] = $separate[5];
		$form['job-application']['relationship']['child-field#' . $separate[3]]['#default_value'] = $separate[6];
	}		

	return $form;
}


/*
* Form Validator
*/

function job_mgt_content_types_validate($form, &$form_state) {

	// dpm($form_state['values']);
	
	$allowed = array('email_default','text_default');
	
	//Check whether email field is selected correctly or not
	if($form_state['values']['email-enable']) {
	$parent_ctype = $form_state['values']['posting-type']; 
	$parent_email = $form_state['values']['email-' . $parent_ctype];
	$field_type = field_info_instance('node',$parent_email, $parent_ctype);
		if(!in_array($field_type['display']['default']['type'],$allowed)) {
			form_set_error('email-' . $parent_ctype,'Must be textfield or email field');
		}
	}

	//Check whether email field for Child-type is correct or not
	$child_ctype = $form_state['values']['application-type'];
	$child_email= $form_state['values']['job_application'];
	$field_type = field_info_instance('node', $child_email, $child_ctype);
	if(!in_array($field_type['display']['default']['type'], $allowed)) {
		form_set_error('job_application','Must be textfield or email field');
	}

	// error_log("Validator Called");
}

/*
* Form Handler
*/

function job_mgt_content_types_submit($form, &$form_state) {
	// dpm($form_state['values']);

	// check_plain() or filter_xss()
	$ctypes = array();

	$posting_type = $form_state['values']['posting-type'];
	$application_type = $form_state['values']['application-type'];

	$poster_email_enable = $form_state['values']['email-enable'];
	$poster_email = check_plain($form_state['values']['email-' . $posting_type]);

	$application_email = $form_state['values'][$application_type];
	$parent_field = $form_state['values']['parent-field#' . $posting_type];
	$child_field = $form_state['values']['child-field#' . $application_type];

	array_push($ctypes,$posting_type . '#' . $poster_email_enable . '#' . $poster_email . '#' . $application_type . '#' . $application_email . '#' . $parent_field . '#' . $child_field);
	variable_set('job_mgt_ctypes',$ctypes);

	drupal_flush_all_caches();
	drupal_set_message('Settings Saved');
}

/* Tutorial */
function job_mgt_tutorial($form, &$form_state) {

	$mod_path = drupal_get_path('module', 'job_mgt');
	$tutorial_path = $GLOBALS['base_url'] . '/' . $mod_path . '/tutorial/_tutorial(JobManagementTool).pdf';
	$form = array();
	$form['intro'] = array(
		'#type' => 'markup',
		'#markup' => '<object data="' . $tutorial_path . '" type="application/pdf" width="100%" height="100%" >
 
  <p>It seems like you don\'t have a PDF plugin for this browser.
  No problem sir... you can <a href="' . $tutorial_path . '">click here to
  download the PDF file.</a></p>
  
</object>',
	'#prefix' => '<div style = "width:100%; height : 100%">',
	'#suffix' => '</div>',
		);
	return $form;
}